/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.robotprojecttt;

/**
 *
 * @author acer
 */
public class TableMap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;
    private Obj object[] = new Obj[100];
    int count = 0;
    private Fuel fuel;
    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void addObject(Obj object) {
        this.object[count] = object;
        count++;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
        addObject(robot);
    }

     public void setFuel(Fuel fuel) {
        this.fuel = fuel;
        addObject(fuel);
    }
    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
        addObject(bomb);
    }

    public void printSymbol(int x, int y) {
        char syn = '-';
        for (int o = 0; o < count; o++) {
            if (object[o].isOn(x,y)) {
                syn=object[o].getSymbol();
            }
        }System.out.print(syn);
    }

    public void showMap() {
        showTitle();
        System.out.println("Fuel: "+robot.getF());
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                printSymbol(x,y);
            }
            showNewLine();

        }

    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        // x -> 0-(width-1), y -> 0-(height-1)
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
    
    public boolean isTree (int x ,int y){
       for(int i =0 ; i<count ;i++){
           if(object[i] instanceof Tree && object[i].isOn(x, y)){
               return true;
           }
       }return false;
    }
    public int fillFuel(int x ,int y){
        for(int i=0;i<count;i++){
            if(object[i] instanceof Fuel && object[i].isOn(x, y)){ 
                return ((Fuel)(object[i])).fillFuel();
            }
        }return 0;
    }
}
