/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.robotprojecttt;

/**
 *
 * @author acer
 */
import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'x',map,20);
        Bomb bomb = new Bomb(5,5);
        
       
        map.addObject(new Tree (2,6));
        map.addObject(new Tree (2,7));
        map.addObject(new Tree (2,9));
        map.addObject(new Tree (3,9));
        map.addObject(new Tree (3,7));
        map.addObject(new Tree (4,5));
        map.addObject(new Fuel (8,8,20));
        map.setRobot(robot);
        map.setBomb(bomb);
        
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}

