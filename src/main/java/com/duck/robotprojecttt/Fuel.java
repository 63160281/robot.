/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.robotprojecttt;

/**
 *
 * @author acer
 */
public class Fuel extends Obj{
    private int vol;
    public Fuel (int x ,int y , int vol){
        super('F',x,y);
        this.vol=vol;
    }

    public int fillFuel() {
        int vol = this.vol;
        this.vol=0;
        symbol='-';
        return vol;
    }
    
}

